CKI theme
=========

Hugo theme based on [purehugo](http://dplesca.github.io/purehugo/),
specifically for the CKI blog.

### Syntax Highlighting

Syntax highlighting is enabled by default and uses the [Rainbow](http://craig.is/making/rainbows) highlighting library. All you need to do is add a language identifier to a code block.

For example, to apply Go syntax highlighting in Markdown:

    ```go
        package main

        import "fmt"

        func main() {
          fmt.Println("Hello, 世界")
        }
    ```

### Responsive Images

For responsive images you could use the built-in responsive image shortcode (without the `/**/` characters):  
```
{{%/* img-responsive "http://example.com/image.jpg" */%}}
```

### Hide Share Options

If you would like to hide the share options in the single post view, you can add this option in the `params` section of your config file.

```toml
[params]
  # ... other options ...
  hideShareOptions = true
```

### Hide Sidebar icons text Options


If you would like to hide the text next to the icons on the sidebar, you can add this option in the `params` section of your config file.

```toml
[params]
  # ... other options ...
  hideSidebarIconText = true
```

### Screenshot
![Screenshot](http://i.imgur.com/Dsj41Rz.png)
