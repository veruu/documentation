---
title: "CKI hackfest agenda for Plumbers"
date: "2019-07-10"
slug: "hackfest-agenda"
author: Veronika Kabatova
---

Following the hackfest invite from the previous post, we're here with more
detailed information!

# Schedule

September 12:

|  Time slot  |               Topic               |
|:-----------:|:---------------------------------:|
| 09:30-10:00 |      **Breakfast** (provided)     |
| 10:00-10:30 |           Introductions           |
| 10:30-11:00 |     Test data standardization     |
| 11:10-12:00 | Common place for upstream results |
| 12:10-13:30 |  How to avoid effort duplication  |
| 13:30-14:30 |   **Lunch break** (not provided)  |
| 14:30-16:00 |      Open testing philosophy      |
| 16:10-17:00 |       Common hardware pools       |

September 13:

|  Time slot  |                   Topic                   |
|:-----------:|:-----------------------------------------:|
| 09:30-10:00 |          **Breakfast** (provided)         |
| 10:00-11:20 | Getting results to developers/maintainers |
| 11:30-12:30 |       Onboarding new trees and tests      |
| 12:40-13:30 |              CI bug tracking              |
| 13:30-14:30 |       **Lunch break** (not provided)      |
| 14:30-16:00 |       Bugs and result interpretation      |
| 16:10-17:30 |  Security when testing untrusted patches  |

The time slots aren't set in stone. Sessions might run longer than planned
(based on how much there is to discuss and plan) and thus the whole hackfest
might end later as well. Discussions and implementations can (and should)
continue on mailing lists and code repositories based on created agreements.

# Topic summaries

* **Test data standardization**: Short session about the current status of test
  result unification across CI systems, what defines test data and metadata.
* **Common place for upstream results**: There are plans to drop all available
  data into a common DB to use (needs previous session), but what next? The
  place should be easily available and browsable by people wanting to see the
  results.
* **How to avoid effort duplication**: How to get results from enough diverse
  environments (HW, compilers, configurations etc.) without duplicating others'
  work and still providing useful results.
* **Open testing philosophy**: Modularity and interoperability of different CIs:
  standardized API points to "share" different parts of CI systems, resource
  usage optimization, bisections, ... see [the modularity document] for more
  details.
* **Common hardware pools**: Sharing hard-to-get HW with other CIs, companies
  being able to sign up to have stuff run on their HW to validate it.
* **Getting results to developers/maintainers**: How many emails to send and
  with what data (links to dashboard, how to reproduce the results, etc.). How
  much data should be sent in the first step and what is OK to be a few clicks
  away? Using Patchwork checks for tested patches that point to dashboard.
* **Onboarding new trees and tests**: What trees need the most attention/are
  most bug-prone? Which tests are you missing in CI but find important? Upstream
  test location unification; making it easy to wrap and onboard tests to CI.
* **CI bug tracking**: Is bugzilla.kernel.org useful to report bugs from CI? Is
  anyone actually checking and fixing stuff that's there in a timely manner?
  Would it make sense to have a bug tracker for possible issues found by CI?
  Topic inspired by [squashing bugs thread] about syzbot bugs.
* **Bugs and result interpretation**: Test result interpretation, regression/fix
  detection, infrastructure issue detection, using neural networks to detect and
  categorize issues, "known issue" detection...
* **Security when testing untrusted patches**: How do we merge, compile, and
  test kernels that have untrusted code in them and have not yet been reviewed?
  How do we avoid abuse of systems, information theft, or other damage?

# Location

The hackfest will take place at **Sana Hotel, Av. Fontes Pereira de Melo 8,
1069-310 Lisboa, Portugal**. Please note that you have to be a confirmed
attendee to join!

[the modularity document]: https://docs.google.com/document/d/15F42HdHTO6NbSL53_iLl77lfe1XQKdWaHAf7XCNkKD8/edit
[squashing bugs thread]: https://lists.linuxfoundation.org/pipermail/ksummit-discuss/2019-May/006389.html
