The source code for [cki-project.org](https://cki-project.org/).

It uses [Hugo](https://gohugo.io/) with a custom theme (see
`themes/cki`) based on `purehugo`.

# Development

To compile the site and serve it, simply type `hugo server` and then
visit http://localhost:8000 with your browser. It's very convenient
when developing the site because it will compile any changes you make
and reload the page automatically in your browser.

The actual compiled site, if you need the files, will
be under `public/`.
